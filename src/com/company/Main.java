package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //Ввести n строк с консоли. Вывести на консоль те строки, которые содержат ключевое слово
        // (ключевое слово задано изначально, например “tree”).
        Scanner scan = new Scanner(System.in);
        System.out.println("введите количество строк, которое Вы желаете ввести: ");
        int numRows = Integer.parseInt(scan.nextLine());

        String[] arrayOFRows = new String[numRows];

        for (int i = 0; i < numRows; i++) {
            System.out.printf("Введите строку %d: ", i + 1);
            String row = scan.nextLine();
            arrayOFRows[i] = row;
        }

        String keyWord = "tree";
        for (int i = 0; i<numRows; i++){
            if (arrayOFRows[i].contains(keyWord)){
                System.out.println("строка, содержащая клюяевое слово: " + arrayOFRows[i]);
            }
        }
    }
}
